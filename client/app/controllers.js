/**
 * All feedbacks/Delete feedback Controller
 */
app.controller("ListDeleteCtrl", function($scope, $state, myFeedbackFactory) {

    $scope.feedback = {};
    $scope.modal = false;

    // Get All feedbacks
    myFeedbackFactory.allFeedbacks().then(function(res){
        $scope.feedbacks = res.data;
    });

    // Cancel Modal
    $scope.cancelModal = function() {
        $scope.modal = false;
    };

    // Show Delete Modal
    $scope.showDeleteModal = function(id) {
        $scope.modal = true;
        $scope.modal = myFeedbackFactory.editFeedback(id).then(function(res) {
            $scope.feedback = res.data;
        });
    };

    // Delete feedback
    $scope.deleteFeedback = function(id) {
        myFeedbackFactory.deleteFeedback(id).then(function(result) {
            if(result.data.success) {
                $state.go($state.current, {}, { reload: true });
            }
        });
    };

});

/**
 * Feedback Create/Update Controller
 */
app.controller("CreateUpdateCtrl", function($scope, $state, $stateParams, myFeedbackFactory) {

    // get Feedback by id if update page
    if($stateParams.id) {
        myFeedbackFactory.editFeedback($stateParams.id).then(function(res) {
            $scope.feedback = res.data;
        });
    }

    // Create Feedback
    $scope.createFeedback = function() {
        myFeedbackFactory.createFeedback($scope.feedback).then(function(result) {
            if(result.data.success) {
                $scope.feedback = {};
                $state.go("admin-all-feedbacks");
            }
        });
    };

    // Update Feedback
    $scope.updateFeedback = function(feedback) {
        myFeedbackFactory.updateFeedback(feedback).then(function(result) {
            if(result.data.success) {
                $state.go("admin-all-feedbacks");
            }
        });
    };
});
