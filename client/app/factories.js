app.factory("myFeedbackFactory", function($http) {

    return {

        // get all Feedbacks
        allFeedbacks: function() {
            return $http.get("/admin/feedbacks");
        },

        // edit Feedback
        editFeedback: function(id) {
            return $http.get("/admin/feedback/" + id + "/edit");
        },

        // update Feedback
        updateFeedback: function(feedback) {
            return $http.post("/admin/feedback/" + feedback._id + "/update", feedback);
        },

        // delete feedback
        deleteFeedback: function(id) {
            return $http.post("/admin/feedback/" + id + "/delete");
        },

        // create Feedback
        createFeedback: function(feedback) {
            return $http.post("/admin/feedback/create", feedback);
        }

    }
});