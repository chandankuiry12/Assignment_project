var app = angular.module("myFeedback", ["ui.router", "textAngular"]);

app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state("admin-all-feedbacks", {
            url: "/feedbacks",
            templateUrl: "/admin/feedbacks-page",
            controller: "ListDeleteCtrl"
        })
        .state("admin-create-feedback", {
            url: "/feedback/new",
            templateUrl: "/admin/feedback/new",
            controller: "CreateUpdateCtrl"
        })
        .state("admin-update-feedback", {
            url: "/feedback/:id/update",
            templateUrl: "/admin/feedback/update",
            controller: "CreateUpdateCtrl"
        });
});