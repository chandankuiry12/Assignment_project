var app             = require("express");
var router          = app.Router();
var feedback            = require("../models/feedback");
var User            = require("../models/user");

/**
 * Admin Middleware
 */
router.use(function authenticatedOrNot(req, res, next){
    if(req.isAuthenticated()) {
        next();
    } else {
        res.redirect("/login");
    }
});

/**
 * Main app page
 */
router.get("/", function(req, res) {
    res.render("admin/layouts/app.html");
});

/**
 * Render feedback Page
 */
router.get("/feedbacks-page", function(req, res) {
    res.render("admin/feedback/index.html");
});
/**
 * render Create new feedback page
 */
router.get("/feedback/new", function(req, res) {
    res.render("admin/feedback/create.html");
});
/**
 * Render Update feedback Page
 */
router.get("/feedback/update", function(req, res) {
    res.render("admin/feedback/update.html");
});
/**
 * return JSON all feedbacks
 */
router.get("/feedbacks", function(req, res) {
    feedback.find(function(err, feedbacks) {
        if(err) throw(err);

        res.json(feedbacks);
    });
});

// =========================================
//              Angular Routes
// =========================================

/**
 * Angular Render Update feedback
 */
router.get("/feedback/:id/edit", function(req, res) {
    feedback.findById(req.params.id, function(err, feedback) {
        if(err) throw err;

        res.json(feedback);
    });
});
/**
 * Angular Post Create feedback
 */
router.post("/feedback/create", function(req, res) {
    feedback.create(req.body, function(err) {
        if(err) throw err;

        res.json({ success: true })
    });
});
/**
 * Angular Post Update feedback
 */
router.post("/feedback/:id/update", function(req, res) {

    feedback.findByIdAndUpdate(req.params.id, req.body, function(err) {
        if(err) throw err;

        res.json({success: true});
    });
});
/**
 * Angular Post Delete feedback
 */
router.post("/feedback/:id/delete", function(req, res) {
    feedback.findByIdAndRemove(req.params.id, function(err) {
        if(err) throw err;

        res.json({success: true});
    });
});


module.exports = router;