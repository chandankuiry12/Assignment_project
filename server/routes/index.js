var express     = require("express");
var router      = express.Router();
var feedback        = require("../models/feedback");

/**
 * Render Login Page
 * if auth redirect to feedbacks
 */
router.get("/login", function authenticatedOrNot(req, res, next) {
    if(req.isAuthenticated()) {
        res.redirect("/admin#/feedbacks");
    } else {
        next();
    }}, function(req, res) {

    res.render("admin/login", { message: req.flash('error') });
});

/**
 * Logout
 */
router.get("/logout", function(req, res) {
    req.logout();

    res.redirect("/login");
});

/**
 * Render Main Page
 */
router.get("/", function(req, res) {
    feedback.find({}).sort({ created_at: -1 }).exec(function(err, feedbacks) {
        if(err) throw(err);

        res.render("client/index", {
            title: "Chandan Kuiry",
            feedbacks: feedbacks,
            desc: "Company feedback",
            url: "/"
        });
    });
});

/**
 * Render feedback by url param id
 */
router.get("/feedback/programming/:name/:id", function(req, res) {

    var name = req.params.name;
    var id = req.params.id;

    feedback.findById(id, function(req, feedback) {
       res.render("client/feedback/feedback", {
           title: name.split('-').join(' '),
           feedback: feedback,
           desc: feedback.short_desc,
           url: "/feedback/programming/" + name + "/" + id
       })
    });
});

/**
* Render Sitemap.xml
*/
router.get("/sitemap.xml", function(req, res) {
    feedback.find({}).sort({ created_at: -1 }).exec(function(err, feedbacks) {

        if(err) throw(err);

        res.setHeader('content-type', 'application/xml');
        res.render("sitemap", {
            feedbacks: feedbacks
        });
    });
});

// ========================================================
//                          Demos
// ========================================================

router.get("/demos/vanilla-slideshow", function(req, res) {
    res.render("demos/vanilla-slideshow.html");
});

/**
 * Download vanilla slideshow
 */
router.get("/download-vanilla-slideshow", function(req, res) {
    res.download("../client/views/demos/vanilla-slideshow.rar");
});


module.exports = router;