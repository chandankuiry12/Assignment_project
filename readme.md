#Assignment project 

This project is about employee feedback system .
If you want to check my github projects then this is my 
official [github link](https://github.com/chandankuiry).
I tried to complete some part of the project in 7 hours. 

## Features

- Create/Edit/Delete feedback
- admin creation 
- Passport Authentication for Administration
- Bcrypt Password Hash
- Mongodb support 
- Angular ui router
- textAngular Text-Editor
- sass


## Server Side Dependencies

````
"bcrypt": "^0.8.4",
"body-parser": "^1.13.2",
"connect-flash": "^0.1.1",
"cookie-parser": "^1.3.5",
"ejs": "^2.3.3",
"express": "^4.13.1",
"express-session": "^1.11.3",
"mongoose": "^4.1.0",
"morgan": "^1.6.1",
"passport": "^0.2.2",
"passport-local": "^1.0.0"
````

## Client Side Dependencies

````
"textAngular": "~1.4.2",
"bootstrap": "~3.3.5",
"angular": "1.4.3",
"angular-ui-router": "~0.2.15"
````

## Getting Started

Clone Repo

````
git clone https://gitlab.com/chandankuiry12/Assignment_project
````

Npm install dependencies

````
cd server 
npm install

cd client 
npm install
````

Create config.js file in config folder

````
module.exports = {
    'secret': 'SomeSecretString',
    'database': 'mongodb://localhost/yourdatabasename'
};
````
Here I used mlab for mongodb . So to run this project you should connect with internet.check server/config/config.js folder



Start Server

````
cd server 
node app.js
````

## Create Admin User
I have create one admin .

````
username: chandan
password: chandan

````

Otherwise if you want to create your own admin .Use this link after running the server.Here pass the username in `your-name ` position and pass the password in `your-password` position. 

````
http://localhost:3000/test/create-user/your-name/your-password
````

this will create admin user with your name and hashed password

## User Admin

After creating admin navigate to 

````
http://localhost:3000/login
````
Insert your newly created name and password



## create feedback

Navigate to 

````
http://localhost:3000/admin#/feedback/new
````

## update feedback

```
After creating feedback you can see the feedback . Just click on the particular feedback to update it.
```

## Delete feedback


After creating feedback you can see the feedback . Just click on the ``delete`` to delete it.



## Change Styles

Css is written with Sass, you can update scss files with running

````
cd client/public & sass --watch scss:css
````

or if you don't want to use Sass just update

````
client/public/css/app.css
````

## Grunt Packages

````
grunt-contrib-concat
grunt-contrib-watch
````

## Start Grunt

Grant will concatenate admin script and css files and watch for changes

````
cd client
grunt
````

output will look like this

````
Running "concat:js" (concat) task
File app/scripts.js created.

Running "concat:css" (concat) task
File public/css/admin/bundle.css created.

Running "watch" task
Waiting...
````
